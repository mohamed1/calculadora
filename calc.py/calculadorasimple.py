def add2numbers(number1, number2):
    result = number1 + number2
    return result

def subtract2numbers(number1, number2):
    result = number1 - number2
    return result

if __name__ == "__main__":
    print("EJERCICIO CALCULADORA SIMPLE")
    print(f'Sumar 1 + 2 = {add2numbers(1,2)}')
    print(f'Sumar 3 + 4 = {add2numbers(3,4)}')
    print(f'Restar 6 - 5 = {subtract2numbers(6,5)}')
    print(f'Restar 8 - 6 = {subtract2numbers(8,7)}')